import React from 'react';
import { Flex, Image, Heading, Text, Box } from '@chakra-ui/core';

const Brand = () => {
  return (
    <Flex alignItems="center">
      <Image src="/apex.svg" alt="Logo's website" size="80px" />
      <Box ml="10px">
        <Heading fontSize="24px">GitHunt</Heading>
        <Text color="gray.600">Most starred projects on GitHub</Text>
      </Box>
    </Flex>
  );
};
export default Brand;
