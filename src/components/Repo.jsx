import React from 'react';
import {
  Box,
  Flex,
  Image,
  Heading,
  Text,
  Link,
  Stack,
  Button,
} from '@chakra-ui/core';
import { GoStar, GoRepoForked, GoIssueOpened } from 'react-icons/go';
import PropTypes from 'prop-types';

const Repo = ({ isListView }) => {
  return (
    <Flex borderWidth={1} bg="white" p="15px" rounded="5px" alignItems="center">
      <Flex flex={1} flexDir="column">
        {!isListView && (
          <Flex mb="15px">
            <Image w="35px" h="35px" rounded="5px" src="apex.svg" />
            <Box ml="10px">
              <Heading fontSize="16px">Toto</Heading>
              <Text fontSize="13px">View profile</Text>
            </Box>
          </Flex>
        )}
        <Box mb="15px">
          <Box mb="10px">
            <Flex fontSize="19px" fontWeight={700} color="purple.700" mb="3px">
              {isListView && (
                <>
                  <Text as="a" href="http://ldellanegra.com" target="_blank">
                    ldellaneg
                  </Text>
                  &nbsp;/&nbsp;
                </>
              )}
              <Text as="a" href="http://ldellanegra.com" target="_blank">
                Githunt
              </Text>
            </Flex>
            <Text color="gray.600" fontSize="14px">
              Built by &middot;
              <Link
                fontWeight={600}
                href="http://ldellanegra.com"
                target="_blank"
              >
                ldella
              </Link>
              &middot; May 2, 2020
            </Text>
          </Box>
          <Text fontSize="14px" color="gray.900">
            blalbldf flqkf,sdl ,dfk ,sdflsdlkdfs dkldsfs
          </Text>
        </Box>

        <Stack isInline spacing="10px">
          <Button
            as="a"
            leftIcon={GoStar}
            cursor="pointer"
            variant="link"
            fontSize="13px"
            iconSpacing="4px"
            _hover={{ textDecoration: 'none' }}
          >
            9098
          </Button>
          <Button
            as="a"
            leftIcon={GoRepoForked}
            cursor="pointer"
            variant="link"
            fontSize="13px"
            iconSpacing="4px"
            _hover={{ textDecoration: 'none' }}
          >
            9098
          </Button>
          <Button
            as="a"
            leftIcon={GoIssueOpened}
            cursor="pointer"
            variant="link"
            fontSize="13px"
            iconSpacing="4px"
            _hover={{ textDecoration: 'none' }}
          >
            9098
          </Button>
        </Stack>
      </Flex>
      {isListView && (
        <Image w="105px" h="105px" rounded="100%" src="apex.svg" />
      )}
    </Flex>
  );
};

Repo.propTypes = {
  isListView: PropTypes.bool,
};
Repo.defaultProps = {
  isListView: false,
};
export default Repo;
