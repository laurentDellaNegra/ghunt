import React from 'react';
import { Text } from '@chakra-ui/core';

const GroupTitle = () => {
  return (
    <Text fontSize="24px" fontWeight={700}>
      This week
      <Text
        fontSize="15px"
        fontWeight={500}
        color="gray.500"
        ml="5px"
        as="span"
      >
        May 16
      </Text>
    </Text>
  );
};
export default GroupTitle;
