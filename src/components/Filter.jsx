import React, { useState, useEffect } from 'react';
import {
  Select,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Button,
  Stack,
} from '@chakra-ui/core';
import { FaTable, FaList } from 'react-icons/fa';
import PropTypes from 'prop-types';
import languages from '../data/languages';

const Filter = ({ onViewChange }) => {
  const [viewType, setViewType] = useState('grid');

  useEffect(() => {
    onViewChange(viewType);
  }, [viewType, onViewChange]);

  return (
    <Stack isInline>
      <Select>
        {languages.map((l) => (
          <option value={l.name} key={l.id}>
            {l.name}
          </option>
        ))}
      </Select>

      <Menu>
        <MenuButton
          as={Button}
          bg="white"
          borderWidth={1}
          px="40px"
          fontWeight={400}
          leftIcon="calendar"
          _focus={{ boxShadow: 'none' }}
        >
          Actions
        </MenuButton>
        <MenuList>
          <MenuItem>Download</MenuItem>
          <MenuItem>Create a Copy</MenuItem>
          <MenuItem>Mark as Draft</MenuItem>
          <MenuItem>Delete</MenuItem>
          <MenuItem as="a" href="#">
            Attend a Workshop
          </MenuItem>
        </MenuList>
      </Menu>
      <Stack
        isInline
        spacing={0}
        borderWidth={1}
        bg="white"
        rounded="5px"
        alignItems="center"
        ml="10px"
      >
        <Button
          h="100%"
          onClick={() => setViewType('grid')}
          leftIcon={FaTable}
          fontWeight="400"
          roundedRight={0}
          bg={viewType === 'grid' ? 'gray.200' : 'white'}
          _focus={{ boxShadow: 'none' }}
        >
          Grid
        </Button>
        <Button
          h="100%"
          onClick={() => setViewType('list')}
          leftIcon={FaList}
          fontWeight="400"
          roundedRight={0}
          bg={viewType === 'list' ? 'gray.200' : 'white'}
          _focus={{ boxShadow: 'none' }}
        >
          List
        </Button>
      </Stack>
    </Stack>
  );
};
Filter.propTypes = {
  onViewChange: PropTypes.func.isRequired,
};
export default Filter;
