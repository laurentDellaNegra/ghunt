import React, { useState } from 'react';
import { Box, Flex, SimpleGrid, Button } from '@chakra-ui/core';
import PageHeader from './components/PageHeader';
import GroupTitle from './components/GroupTitle';
import Filter from './components/Filter';
import Repo from './components/Repo';

const Feed = () => {
  const [viewType, setViewType] = useState();
  return (
    <Box maxWidth="1200px" mx="auto">
      <PageHeader />

      <Flex alignItems="center" justifyContent="space-between" mb="25px">
        <GroupTitle />
        <Filter
          onViewChange={(vt) => {
            setViewType(vt);
          }}
        />
      </Flex>

      <SimpleGrid columns={viewType === 'list' ? 1 : 3} spacing="15px">
        <Repo isListView={viewType === 'list'} />
        <Repo isListView={viewType === 'list'} />
        <Repo isListView={viewType === 'list'} />
        <Repo isListView={viewType === 'list'} />
        <Repo isListView={viewType === 'list'} />
        <Repo isListView={viewType === 'list'} />
        <Repo isListView={viewType === 'list'} />
        <Repo isListView={viewType === 'list'} />
        <Repo isListView={viewType === 'list'} />
        <Repo isListView={viewType === 'list'} />
        <Repo isListView={viewType === 'list'} />
      </SimpleGrid>
      <Flex alignItems="center" justifyContent="center" my="20px">
        <Button variantColor="blue">Load Next group</Button>
      </Flex>
    </Box>
  );
};
export default Feed;
