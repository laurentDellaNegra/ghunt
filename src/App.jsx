import React from 'react';
import { Box } from '@chakra-ui/core';
import Feed from './Feed';

export default function App() {
  return (
    <Box minHeight="100vh" bg="gray.100">
      <Feed />
    </Box>
  );
}
